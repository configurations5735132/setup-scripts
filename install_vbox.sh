#!/bin/bash

pacman --ask 4 --needed -Sy virtualbox || echo "failed to install virtualbox" >&2 exit 2
pacman --ask 4 --needed -Sy virtualbox-host-dkms || echo "failed to install virtualbox" >&2 exit 2

dkms autoinstall || echo "failed to set up vbox" >&2 exit 2
modprobe vboxdrv || echo "failed to set up vbox" >&2 exit 2
