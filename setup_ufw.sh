#!/bin/bash


if command -v ufw &> /dev/null; then # check if ufw is installed
  echo "ufw is not installed"
fi


# set default behavior of incoming trafic
ufw default deny incoming
# set default behavior of outgoing trafic
ufw default allow outgoing

# allow http/https
ufw allow http
ufw allow https

# allow ssh
# ufw allow ssh

# limit SSH on pi server (if someone tries to ssh into the computer to often it will block its ip)
ufw limit SSH

# allow NTP for the system time
ufw allow ntp

# allow DHCP. (if now static IP is used)
ufw allow 67:68/tcp

# allow dns trafic
# sudo ufw allow 53

# enable trafic throw tor network
ufw allow 56881:56889/tcp

# ports to allow for steam
# ufw allow 27000:27036/udp
# ufw allow 27036:27037/tcp
# ufw allow 4380/udp


# enable system service
systemctl enable ufw.service --now

# start firewall
ufw enable
